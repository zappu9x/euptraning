package com.example.nguye.euptrainingproject1.activities;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.adapters.MoviePagerAdapter;

@SuppressLint("NewApi")
public class AnimationActivity extends AppCompatActivity implements View.OnClickListener, View.OnScrollChangeListener {

    private TextView mPositionTextView;
    private ViewPager mPager;
    private Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);

        setUpView();
    }

    public void setUpView() {
        mPositionTextView = (TextView) findViewById(R.id.position);
        mPager = (ViewPager) findViewById(R.id.pager);

        findViewById(R.id.previous).setOnClickListener(this);
        findViewById(R.id.next).setOnClickListener(this);

        mPager.setAdapter(new MoviePagerAdapter(getSupportFragmentManager()));
        mPager.setOnScrollChangeListener(this);
        mPositionTextView.setText(mPager.getCurrentItem()+1 + "/" + mPager.getAdapter().getCount());
    }

    @Override
    public void onClick(View v) {
        int position = 0;
        switch (v.getId()) {
            case R.id.previous:
                position = (mPager.getCurrentItem()+(mPager.getAdapter().getCount()-1))%mPager.getAdapter().getCount();
                break;
            case R.id.next:
                position = (mPager.getCurrentItem()+1)%mPager.getAdapter().getCount();
                break;
            default:
                break;
        }
        mPager.setCurrentItem(position);

    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        mPositionTextView.setText(mPager.getCurrentItem()+1 + "/" + mPager.getAdapter().getCount());
    }
}
