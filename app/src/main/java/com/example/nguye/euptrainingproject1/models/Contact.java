package com.example.nguye.euptrainingproject1.models;

/**
 * Created by nguye on 3/18/2016.
 */
public class Contact {
    public String id;
    public String name;
    public String email;
    public String address;
    public String gender;
    Phone phone;

    public class Phone {
        public String mobile;
        public String home;
        public String office;
    }
}
