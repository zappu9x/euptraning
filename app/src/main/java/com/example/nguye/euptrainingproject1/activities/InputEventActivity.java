package com.example.nguye.euptrainingproject1.activities;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.nguye.euptrainingproject1.R;

public class InputEventActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener, View.OnFocusChangeListener, View.OnKeyListener, View.OnTouchListener {

    private Context mContext = this;

    private Button mButton;
    private EditText mEditText;
    private ImageView mImageView;
    private float dX, dY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_event);

        setUpView();
        setListenerView();

    }

    private void setUpView(){
        mButton = (Button) findViewById(R.id.ie_bt);
        mEditText = (EditText) findViewById(R.id.ie_et);
        mImageView = (ImageView) findViewById(R.id.ie_iv);

    }

    private void setListenerView() {
        mButton.setOnClickListener(this);
        mButton.setOnLongClickListener(this);

        mEditText.setOnFocusChangeListener(this);
        mEditText.setOnKeyListener(this);

        mImageView.setOnTouchListener(this);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(mContext, "Button clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onLongClick(View v) {
        Toast.makeText(mContext, "Button long clicked", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Toast.makeText(mContext, "Focus = " + hasFocus, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN &&
                (keyCode == KeyEvent.KEYCODE_ENTER || keyCode == KeyEvent.KEYCODE_DPAD_CENTER)){

            Toast.makeText(mContext, "Click ENTER key", Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }
    @Override
    public boolean onTouch(View view, MotionEvent event) {

        switch (event.getActionMasked()) {

            case MotionEvent.ACTION_DOWN:

                dX = view.getX() - event.getRawX();
                dY = view.getY() - event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                view.animate()
                        .x(event.getRawX() + dX)
                        .y(event.getRawY() + dY)
                        .setDuration(0)
                        .start();
                break;
            default:
                return false;
        }
        return true;
    }
}
