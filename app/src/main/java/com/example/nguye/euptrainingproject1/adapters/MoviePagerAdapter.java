package com.example.nguye.euptrainingproject1.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.nguye.euptrainingproject1.fragments.PagerChildFragment;

public class MoviePagerAdapter extends FragmentPagerAdapter {

    public MoviePagerAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public Fragment getItem(int position) {
        PagerChildFragment fragment = new PagerChildFragment();
        fragment.setPostion(position);
        return fragment;
    }
}
