package com.example.nguye.euptrainingproject1.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.dialogs.CustomDialog;

import java.util.Calendar;

public class ToastActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView mListView;
    private Context mContext = this;
    Calendar calendar = Calendar.getInstance();
    private Toast mToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_toast);

        setUpView();
        setDataView();
    }

    private void setUpView(){
        mListView = (ListView) findViewById(R.id.toast_list);
    }

    private void setDataView(){
        String[] list = {"Toast 1: cung voi toast 3", "Toast 2: khac voi toast 1", "Toast 3: cung voi toast 1"};
        mListView.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list));
        mListView.setOnItemClickListener(this);
        mToast = Toast.makeText(this, "", Toast.LENGTH_SHORT);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                showToast1();
                break;
            case 1:
                showToast2();
                break;
            case 2:
                showToast3();
                break;
            default:
                break;
        }
    }

    public void showToast1(){
        mToast.setText("これはトアト1です。");
        mToast.show();
    }

    public void showToast2(){
       Toast.makeText(this, "これはトアト2です。(toast khac)", Toast.LENGTH_SHORT).show();

    }
    public void showToast3(){
        mToast.setText("これはトアト3です。");
        mToast.show();
    }


}
