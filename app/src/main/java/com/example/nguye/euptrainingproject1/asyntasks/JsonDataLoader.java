package com.example.nguye.euptrainingproject1.asyntasks;

import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.nguye.euptrainingproject1.adapters.ContactAdapter;
import com.example.nguye.euptrainingproject1.models.Contact;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class JsonDataLoader extends AsyncTask<String, Void, String> {

    private Context mContext;
    private ListView mListView;
    private ProgressBar mProgressBar;
    public JsonDataLoader(Context context, ListView listView, ProgressBar progressBar){
        mContext = context;
        mListView = listView;
        mProgressBar = progressBar;
    }
    @Override
    protected void onPreExecute() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection urlConnection = null;
        String result = "";
        try {
            URL url = new URL(params[0]);
            urlConnection = (HttpURLConnection) url.openConnection();
            int code = urlConnection.getResponseCode();
            if(code==200){
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                if (in != null) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                    String line = "";

                    while ((line = bufferedReader.readLine()) != null)
                        result += line;
                }
                in.close();
            }

            return result;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        finally {
            urlConnection.disconnect();
        }
        return result;

    }

    @Override
    protected void onPostExecute(String result) {
        try {
            setDataFromResult(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        super.onPostExecute(result);
    }

    public void setDataFromResult(String result) throws JSONException {
        mProgressBar.setVisibility(View.INVISIBLE);
        List<Contact> contactList = new ArrayList<>();
        JSONObject jsonObject = new JSONObject(result);
        JSONArray jsonArray = jsonObject.getJSONArray("contacts");
        for (int i = 0; i < jsonArray.length(); i++) {
            String obj = jsonArray.getString(i);
            Gson gson = new GsonBuilder().create();
            Contact contact = gson.fromJson(obj, Contact.class);
            contactList.add(contact);
        }
        if(!contactList.isEmpty()) mListView.setAdapter(new ContactAdapter(mContext, contactList));
    }
}