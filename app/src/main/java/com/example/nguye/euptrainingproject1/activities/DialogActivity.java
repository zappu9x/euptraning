package com.example.nguye.euptrainingproject1.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.dialogs.CustomDialog;
import com.example.nguye.euptrainingproject1.utils.Constant;

import java.util.Calendar;

public class DialogActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, DialogInterface.OnClickListener {

    private ListView mListView;
    private Context mContext = this;
    Calendar calendar = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);

        setUpView();
        setDataView();
    }

    private void setUpView(){
        mListView = (ListView) findViewById(R.id.dialog_list);
    }

    private void setDataView(){
        String[] list = {"Custom dialog", "AlertDialog", "DatePickerDialog", "TimePickerDialog"};
        mListView.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, list));
        mListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case 0:
                showCustomDialog();
                break;
            case 1:
                showAlertDialog();
                break;
            case 2:
                showDatePickerDialog();
                break;
            case 3:
                showTimePickerDialog();
                break;
            default:
                break;
        }
    }

    public void showCustomDialog(){
        CustomDialog customDialog = new CustomDialog(this);
        customDialog.show();
    }

    public void showAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        DialogInterface.OnClickListener mDialogListener = this;
        builder.setPositiveButton("OK", mDialogListener);
        builder.setNegativeButton("CANCEL", mDialogListener);
        AlertDialog alertDialog = builder.create();
        alertDialog.setTitle("Alert Dialog");
        alertDialog.setMessage("This is conntent dialog");
        alertDialog.show();
    }

    public void showDatePickerDialog(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, this,
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
        datePickerDialog.show();
    }
    public void showTimePickerDialog(){
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, this,
                calendar.get(Calendar.HOUR), calendar.get(Calendar.MINUTE), true);
        timePickerDialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Toast.makeText(this, "Date : " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Toast.makeText(this, "Time : " + hourOfDay + "h" + minute, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
    }
}
