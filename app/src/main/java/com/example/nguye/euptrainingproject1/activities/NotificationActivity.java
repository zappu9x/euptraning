package com.example.nguye.euptrainingproject1.activities;

import android.app.DatePickerDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.dialogs.CustomDialog;

import java.util.Calendar;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView mPush, mCancel;
    private Context mContext = this;
    private NotificationManager mNotificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        setUpView();
    }

    public void setUpView(){
        mPush = (TextView) findViewById(R.id.push_bt);
        mCancel = (TextView) findViewById(R.id.cancel_bt);

        mPush.setOnClickListener(this);
        mCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.push_bt) pushNotification();
        else cancelNotification();
    }

    public void pushNotification(){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Eup notification")
                .setContentText("This is content notfication");
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        int notifyID = 1;
        mNotificationManager.notify(notifyID, builder.build());
    }
    public void cancelNotification(){
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // Sets an ID for the notification, so it can be updated
        int notifyID = 1;
        mNotificationManager.cancel(notifyID);
    }
}
