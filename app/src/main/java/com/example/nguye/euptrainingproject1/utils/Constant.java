package com.example.nguye.euptrainingproject1.utils;

/**
 * Created by nguye on 3/15/2016.
 */
public interface Constant {

    String LAYOUT_STRING = "Layout";
    int LAYOUT_POS = 0;

    String INCONTROL_STRING = "Input control";
    int INCONTROL_POS = 1;

    String INEVENT_STRING = "Input event";
    int INEVENT_POS = 2;

    String DIALOG_STRING = "Dialog";
    int DIALOG_POS = 3;

    String NOTI_STRING = "Notification";
    int NOTI_POS = 4;

    String TOAST_STRING = "Toast";
    int TOAST_POS = 5;

    String MAIN_LIST[] = {LAYOUT_STRING, INCONTROL_STRING, INEVENT_STRING, DIALOG_STRING, NOTI_STRING, TOAST_STRING};


}
