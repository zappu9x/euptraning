package com.example.nguye.euptrainingproject1.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.utils.Constant;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView mMainListView;
    private Context mContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpView();
        setDataView();
    }

    private void setUpView(){
        mMainListView = (ListView) findViewById(R.id.main_list);
    }

    private void setDataView(){
        mMainListView.setAdapter(new ArrayAdapter<String>(mContext, android.R.layout.simple_list_item_1, Constant.MAIN_LIST));
        mMainListView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch (position){
            case Constant.LAYOUT_POS:
                startActivity(new Intent(mContext, LayoutActivity.class));
                break;
            case Constant.INCONTROL_POS:
                startActivity(new Intent(mContext, InputControlActivity.class));
                break;
            case Constant.INEVENT_POS:
                startActivity(new Intent(mContext, InputEventActivity.class));
                break;
            case Constant.DIALOG_POS:
                startActivity(new Intent(mContext, DialogActivity.class));
                break;
            case Constant.TOAST_POS:
                startActivity(new Intent(mContext, ToastActivity.class));
                break;
            case Constant.NOTI_POS:
                startActivity(new Intent(mContext, NotificationActivity.class));
                break;
            default:
                break;
        }
    }
}
