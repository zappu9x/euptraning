package com.example.nguye.euptrainingproject1.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.asyntasks.JsonDataLoader;
import com.example.nguye.euptrainingproject1.asyntasks.ProgressAsyntask;

import java.util.ArrayList;
import java.util.List;

public class JsonActivity extends AppCompatActivity implements View.OnClickListener {
    private ListView mList;
    private TextView mStart;
    private ProgressBar mProgressBar;
    private List contactList = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json);

        setUpView();
        setDataView();
    }

    private void setUpView() {
        mStart = (TextView) findViewById(R.id.js_start_bt);
        mList = (ListView) findViewById(R.id.json_list);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
    }

    private void setDataView() {
        mStart.setOnClickListener(this);
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        JsonDataLoader loader = new JsonDataLoader(this, mList, mProgressBar);
        loader.execute("http://api.androidhive.info/contacts/.");
    }
}
