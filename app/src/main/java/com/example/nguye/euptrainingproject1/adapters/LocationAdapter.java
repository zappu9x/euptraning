package com.example.nguye.euptrainingproject1.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Created by nguye on 3/15/2016.
 */
public class LocationAdapter extends ArrayAdapter {
    public LocationAdapter(Context context, String[] arr) {
        super(context, android.R.layout.simple_spinner_item, arr);
        setDropDownViewResource(android.R.layout.simple_list_item_1);
    }
}
