package com.example.nguye.euptrainingproject1.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.adapters.LocationAdapter;

public class InputControlActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener, DialogInterface.OnClickListener, AdapterView.OnItemSelectedListener {

    private EditText mEditText;
    private CheckBox mHideTextCheck;
    private Spinner mLocationSpiner;
    private RadioButton mEngButton, mViButton, mJaButton;
    private Switch mSwitch;
    private TextView mSendButton;
    private String mLanguage = "No language";
    private String mHave = "Havent";
    private String mLocation = "";
    private AlertDialog mDialog;
    private AlertDialog.Builder mAlertDialogBuilder;
    private DialogInterface.OnClickListener mDialogListener;
    private LocationAdapter mAdapter;
    private String locations[]={"Hà Nội", "Nghệ An", "Hải Dương", "Quảng Bình", "Bến Tre"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_control);

        setUpView();
        setDataView();
        setListenerView();
    }

    private void setUpView(){
        mEditText = (EditText) findViewById(R.id.ic_et);
        mHideTextCheck = (CheckBox) findViewById(R.id.ic_cb);
        mLocationSpiner = (Spinner) findViewById(R.id.ic_spiner);
        mEngButton = (RadioButton) findViewById(R.id.ic_eng_rb);
        mViButton = (RadioButton) findViewById(R.id.ic_vi_rb);
        mJaButton = (RadioButton) findViewById(R.id.ic_ja_rb);
        mSwitch = (Switch) findViewById(R.id.ic_switch);
        mSendButton = (TextView) findViewById(R.id.ic_bt);

    }
    private void setDataView(){
        mAlertDialogBuilder = new AlertDialog.Builder(this);
        mAdapter = new LocationAdapter(this, locations);
        mLocationSpiner.setAdapter(mAdapter);
    }
    private void setListenerView(){

        mHideTextCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                hideText(isChecked);
            }
        });
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                switchHave(isChecked);
            }
        });

        mEngButton.setOnCheckedChangeListener(this);
        mViButton.setOnCheckedChangeListener(this);
        mJaButton.setOnCheckedChangeListener(this);
        mSendButton.setOnClickListener(this);
        mLocationSpiner.setOnItemSelectedListener(this);
        mDialogListener = this;

        mAlertDialogBuilder.setPositiveButton("OK", mDialogListener);
        mAlertDialogBuilder.setNegativeButton("CANCEL", mDialogListener);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(!isChecked) return;
        switch (buttonView.getId()){
            case R.id.ic_eng_rb:
                mLanguage = "English";
                break;
            case R.id.ic_vi_rb:
                mLanguage = "Tiếng việt";
                break;
            case R.id.ic_ja_rb:
                mLanguage = "日本語";
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        mDialog = mAlertDialogBuilder.create();
        mDialog.setTitle("Dialog test");
        mDialog.setMessage("Text : " + mEditText.getText().toString() + "\n" +
                "Location : " + mLocation+ "\n" +
                "Language : " + mLanguage + "\n" +
                mHave + " girl friend");
        mDialog.show();
    }


    private void hideText(boolean isChecked){
        mEditText.setTransformationMethod(!isChecked ?
                HideReturnsTransformationMethod.getInstance() :
                PasswordTransformationMethod.getInstance());
    }
    private void switchHave(boolean isChecked){
        mHave = isChecked ? "Have" : "Havent";
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        dialog.dismiss();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        mLocation = ((TextView) view).getText().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
