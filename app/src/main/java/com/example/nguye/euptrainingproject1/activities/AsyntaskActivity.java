package com.example.nguye.euptrainingproject1.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.nguye.euptrainingproject1.R;
import com.example.nguye.euptrainingproject1.asyntasks.ProgressAsyntask;

public class AsyntaskActivity extends AppCompatActivity implements View.OnTouchListener, View.OnClickListener {
    private ProgressBar mProgressBar;
    private TextView mStart;
    private Button mMove;
    private float xPos, yPos;
    private float xDown, yDown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asyntask);

        setUpView();
        setDataView();
    }

    private void setUpView(){
        mProgressBar = (ProgressBar) findViewById(R.id.as_pb);
        mStart = (TextView) findViewById(R.id.as_start_bt);
        mMove = (Button) findViewById(R.id.as_move_bt);
    }

    private void setDataView(){
        mProgressBar.setMax(100);
        mProgressBar.setProgress(0);
        mProgressBar.setVisibility(View.INVISIBLE);
        mStart.setOnClickListener(this);
        mMove.setOnTouchListener(this);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getActionMasked()){
            case MotionEvent.ACTION_DOWN:
                xPos = v.getX();
                yPos = v.getY();
                xDown = event.getRawX();
                yDown = event.getRawY();
                break;
            case MotionEvent.ACTION_MOVE:
                if(event.getRawX() != xDown || event.getRawY() != yDown){
                    float dx = event.getRawX() - xDown;
                    float dy = event.getRawY() - yDown;
                    v.animate()
                            .x(xPos + dx)
                            .y(yPos + dy)
                            .setDuration(0)
                            .start();
                }
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;

        }
        return false;
    }

    @Override
    public void onClick(View v) {
        new ProgressAsyntask(mProgressBar).execute();
    }
}
